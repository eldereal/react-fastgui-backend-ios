"use strict";

import { GUIComponent } from 'react-fastgui';
import { AppRegistry, NavigatorIOS, StyleSheet } from 'react-native';
import React, { Component } from 'react';

const commonStyle = StyleSheet.create({
    flex: {
        flex: 1
    },
    pageWithoutNavigator: {
        flex: 1,
        paddingTop: 20
    },
    pageWithNavigator: {
        flex: 1,
        paddingTop: 64
    }
});


export default class Navigator {

    constructor(backend){
        this.backend = backend;
    }

    init(ongui, options){
        this.options = options;
        this.navigator = new Promise((fulfill, reject) => {



            if(!options.appName){
                throw new Error("options.appName is required for ios backend. It will be passed to AppRegistry.registerComponent(options.appName, _)");
            }

            const props = {
                id: options.rootComponentId || "__root",
                backend: this.backend,
                ongui: ongui,
                options: options,
                style: [
                    options.theme && options.theme.styles && options.theme.styles.Page,
                    commonStyle.pageWithoutNavigator
                ]
            };

            class RootPageComponent extends GUIComponent {
                static get defaultProps(){
                    return props;
                }
            }

            const name = ongui.displayName || ongui.name;

            class IOSStartComponent extends Component {

                componentDidMount(){
                    // console.info("IOSStartComponent", this.refs.navigator);
                    fulfill(this.refs.navigator);
                }

                render(){
                    return <NavigatorIOS
                    ref="navigator"
                    initialRoute={{
                        component: RootPageComponent,
                        title: options.appTitle || name || "Home",
                        passProps: props,
                        navigationBarHidden: !options.appTitle
                    }} style={ commonStyle.flex }/>;
                }
            }

            AppRegistry.registerComponent(options.appName, () => IOSStartComponent);
        });
    }

    replace(item){
        this.navigator.then(nav => {
            const name = item.displayName || item.name;
            const id = name + "-" + Math.random().toString(36).substr(-6);
            const props = {
                id: id,
                backend: this.backend,
                ongui: item,
                options: this.options,
                style: [
                    commonStyle.pageWithoutNavigator
                ]
            };

            class RootPageComponent extends GUIComponent {

                static get defaultProps(){
                    return props;
                }
            }

            nav.replace({
                component: RootPageComponent,
                passProps: props,
                navigationBarHidden: true,
                title: name
            });
        });
    }

    push(item){
        this.navigator.then(nav => {
            const name = item.displayName || item.name;
            const id = name + "-" + Math.random().toString(36).substr(-6);
            const props = {
                id: id,
                backend: this.backend,
                ongui: item,
                options: this.options,
                style: [
                    commonStyle.pageWithoutNavigator
                ]
            };

            class RootPageComponent extends GUIComponent {

                static get defaultProps(){
                    return props;
                }
            }

            nav.push({
                component: RootPageComponent,
                passProps: props,
                navigationBarHidden: true,
                title: name
            });
        });
    }

    pop(result){
        this.navigator.then(nav => {
            nav.pop();
        });
    }

    canPop(){

    }
}
