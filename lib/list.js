"use strict";

import React, { createComponent } from 'react-fastgui';
import { View, ListView } from 'react-native';
import Group from './group';
import processStyle from './processStyle';

export default createComponent(function *list(props, children){

    const dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 != r2
    }).cloneWithRows(children);

    yield <ListView
        { ...props }
        style={ processStyle(props.style) }
        dataSource={ dataSource }
        renderRow = {item => {
            if(Array.isArray(item)){
                var newSource = {
                    fileName: props.__source.fileName,
                    lineNumber: props.__source.lineNumber + ":group"
                };
                return React.createElement(Group, { __source: newSource }, ...item);
            }else{
                return item;
            }
    }}/>;
});
