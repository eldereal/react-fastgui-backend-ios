"use strict";

import React, { createComponent } from 'react-fastgui';
import { Text } from 'react-native';
import processStyle from './processStyle';

export default createComponent(function *label(props, children){
    yield <Text
        {...props}
        style={ processStyle(props.style) }
        >
        {children}
    </Text>;
});
