"use strict";

import React from 'react';
import { StyleSheet, Text } from 'react-native';

import button from './button';
import group from './group';
import label from './label';
import list from './list';
import page from './page';
import image from './image';
import textbox from './textbox';
import bootstrap from './bootstrap';
import Navigator from './navigator';

const RawTextCount = Symbol("RawTextCount");

const backend = {
    components: {
        Button: button,
        Group: group,
        Label: label,
        List: list,
        Image: image,
        Page: page,
        TextBox: textbox
    },
    rawText: function*(...textlist){
        const idx = this[RawTextCount] || 0;
        this[RawTextCount] = idx + 1;
        yield <Text key={ idx } >{ textlist }</Text>;
    },
    StyleSheet: StyleSheet,
    bootstrap: bootstrap
};

backend.navigator = new Navigator(backend);

export default backend;
