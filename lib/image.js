"use strict";

import React, { createComponent } from 'react-fastgui';
import { Image } from 'react-native';
import processStyle from './processStyle';

export default createComponent(function *image(props, children){
    // console.info("Image", props, children);
    yield <Image
        {...props}
        style={ processStyle(props.style) }
        >
        {children}
    </Image>;
});
