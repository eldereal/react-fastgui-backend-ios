"use strict";

import React, { createComponent } from 'react-fastgui';
import { View } from 'react-native';
import processStyle from './processStyle';

export default createComponent(function *group(props, children){
    yield <View
        { ...props }
        style={ processStyle(props.style) }
        >
            { children }
        </View>;
});
