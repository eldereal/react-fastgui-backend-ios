"use strict";

import React, { createComponent } from 'react-fastgui';
import { TextInput } from 'react-native';
import blacklist from 'blacklist';
import processStyle from './processStyle';

function* TextBox(props, children){
    let text = this.persist("text") || "";
    // console.info("persist text", text);
    const evtText = this.transient("text");
    // console.info("transient text", evtText);
    const newText = evtText !== undefined ? evtText : props.value;
    if(newText !== undefined && newText !== text){
        text = newText;
        this.persist("text", text);
    }

    // const nprops = blacklist(props, "value", "onChangeText");
    if(children && children.length){
        yield <TextInput
            onChangeText={(text)=>{
                this.transient("text", text);
                this.reload();
            }}
            { ...props }
            style={ processStyle(props.style) }
            value={ text }
            >
                {children}
            </TextInput>;
    }else{
        yield <TextInput
            onChangeText={(text)=>{
                this.transient("text", text);
                this.reload();
            }}
            { ...props }
            style={ processStyle(props.style) }
            value={ text }
            />;
    }

    return text;
}

export default createComponent(TextBox);
