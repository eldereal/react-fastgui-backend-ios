"use strict";

import { StyleSheet, View, Text, Dimensions as NativeDimensions } from 'react-native';
import { GUIComponent, Dimensions } from 'react-fastgui';
import React, { Component } from 'react';

export default function(ongui, options){

    Dimensions.set({'window': function(){
        const res = NativeDimensions.get('window');
        return res;
    }});

    this.navigator.init(ongui, options);
}
