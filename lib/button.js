"use strict";
/*jshint scripturl:true*/

import React, { createComponent } from 'react-fastgui';
import { TouchableOpacity } from 'react-native';
import extend from 'babel-runtime/helpers/extends';
import processStyle from './processStyle';

export default createComponent(function*(props, children){
    yield <TouchableOpacity
        onPress={() => {
            if(props.onClick){
                props.onClick.call(this, arguments);
            }else{
                this.transient("pressed", true);
                this.reload();
            }
        }}
        {...props}
        style={ processStyle(props.style) }
        >
            { children }
        </TouchableOpacity>;
    return this.transient("pressed");
});
