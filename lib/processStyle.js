"use strict";


import { StyleSheet as FastGuiStyleSheet } from 'react-fastgui';
import { StyleSheet } from 'react-native';

const { isPersistedStyleSheet, getPersistId, setPersistData, getPersistData } = FastGuiStyleSheet;
const ReactNativeStyle = Symbol("ReactNativeStyle");

export default function processStyle(...stylelist){
    if(!stylelist){
        return;
    }
    if(!Array.isArray(stylelist)){
        return processStyle([stylelist]);
    }
    const res = [];
    for(const st of flattenArray(stylelist)){
        if(!st){

        }else if (isPersistedStyleSheet(st)) {
            let persist = getPersistData(st, ReactNativeStyle);
            if (!persist) {
                const id = getPersistId(st);
                persist = StyleSheet.create({[id]: st})[id];
                console.info("create persist style", id, st, persist);
                setPersistData(st, ReactNativeStyle, persist);
            }
            res.push(persist);
        } else {
            res.push(st);
        }
    }
    return res;
}

function* flattenArray(arr){
    for(const item of arr){
        if (Array.isArray(item)) {
            yield* flattenArray(item);
        } else {
            yield item;
        }
    }
}
